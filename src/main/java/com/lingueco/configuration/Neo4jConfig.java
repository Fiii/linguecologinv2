package main.java.com.lingueco.configuration;



import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.neo4j.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.config.Neo4jConfiguration;
import org.springframework.data.neo4j.rest.SpringRestGraphDatabase;

import javax.annotation.Resource;

/**
 * Michal Winnicki
 */
@Configuration
@PropertySource(value="classpath:/properties/database.properties")
@EnableNeo4jRepositories(basePackages="main.java.com.lingueco.repository")
public class Neo4jConfig extends Neo4jConfiguration {

    @Resource
    public Environment env;

    public Neo4jConfig() {
        setBasePackage("com.lingueco.repository");
    }

    @Bean
    public SpringRestGraphDatabase graphDatabaseService() {
        return new SpringRestGraphDatabase(
                        env.getProperty("db.location"),
                        env.getProperty("db.user"),
                        env.getProperty("db.password"));
    }



}
