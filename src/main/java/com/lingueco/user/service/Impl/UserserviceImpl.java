package com.lingueco.user.service.Impl;

import com.lingueco.user.service.Userservice;
import com.lingueco.user.dao.Userdao;
import com.lingueco.user.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Artur
 */
@Service
@Transactional
public class UserserviceImpl implements Userservice {

    @Autowired
    private Userdao userDAO;


    @Override
    public long createUser(User user) {
        return userDAO.createUser(user);
    }

    @Override
    public User updateUser(User user) {
        return userDAO.updateUser(user);
    }

    @Override
    public void deleteUser(long id) {
        userDAO.deleteUser(id);
    }

    @Override
    public List<User> getAllUsers() {
        return userDAO.getAllUsers();
    }

    @Override
    public User getUser(long id) {
        return userDAO.getUser(id);
    }
}
